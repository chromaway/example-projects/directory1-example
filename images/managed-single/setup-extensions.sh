#!/bin/bash

set -eu

if [ -z "$(pmc subnode-images | jq '.[] | select(.Name == "vector_db_extension")')" ]; then
  echo "- Setting up extension: vector db"

  pmc subnode-image add --name vector_db_extension \
    --url $VECTOR_DB_EXTENSION_IMAGE \
    --digest $VECTOR_DB_EXTENSION_DIGEST \
    --image-description "Extension to Postchain for Postgres Vector DB support" \
    -gtx net.postchain.gtx.extensions.vectordb.VectorDbGTXModule
fi

if [ -z "$(pmc subnode-images | jq '.[] | select(.Name == "stork_oracle_chromia_extension")')" ]; then
  echo "- Setting up extension: stork oracle"

  pmc subnode-image add --name stork_oracle_chromia_extension \
    --url $STORK_ORACLE_EXTENSION_IMAGE \
    --digest $STORK_ORACLE_EXTENSION_DIGEST \
    --image-description "Extensions to Postchain for integration against Stork services" \
    -gtx net.postchain.stork.StorkOracleGTXModule \
    -sync net.postchain.stork.StorkOracleSynchronizationInfrastructureExtension
fi

if [ -z "$(pmc subnode-images | jq '.[] | select(.Name == "custom_sql_query")')" ]; then
  echo "- Setting up extension: custom SQL query"

  pmc subnode-image add --name custom_sql_query \
    --url $CUSTOM_SQL_QUERY_EXTENSION_IMAGE \
    --digest $CUSTOM_SQL_QUERY_EXTENSION_DIGEST \
    --image-description "Custom SQL query" \
    -gtx net.postchain.gtx.extensions.customsqlquery.CustomSQLQueryGTXModuleFactory
fi

