#!/bin/bash

set -eu

BRID="$1"
CONTAINER_NAME="$2"

echo "- Setting up pmc"
pmc config --set api.url=http://localhost:7740
pmc config --set privkey=BBBDFE956021912512E14BB081B27A35A0EABC4098CB687E973C434006BCE114
pmc config --set pubkey=03ECD350EEBC617CBBFBEF0A1B7AE553A748021FD65C7C50C5ABB4CA16D4EA5B05
pmc config --set BRID=$BRID

if ! pmc network summary &> /dev/null; then
  echo "- Setting up network"
  pmc network initialize --system-anchoring-config system_anchoring.xml  --cluster-anchoring-config cluster_anchoring.xml
fi

if ! pmc container info -n "$CONTAINER_NAME" &> /dev/null; then
  echo "- Setting up container"
  pmc container add --name "$CONTAINER_NAME" --cluster system --pubkeys "$(pmc config --get pubkey --file .chromia/config)"
fi
