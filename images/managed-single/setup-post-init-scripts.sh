#!/bin/bash

set -eu
shopt -s nullglob

echo "- Running post init scripts"

for file in "$POST_INIT_SCRIPTS"/*; do

  echo "  - Runs $(basename "$file")"

  "$file"
done
