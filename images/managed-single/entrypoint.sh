#!/bin/bash
# Copyright (c) 2022 ChromaWay Inc. See README for license information.

set -eu

# Configuration
export POSTCHAIN_STDOUT_CONTROL_FILE=/tmp/postchain.stdout
export CONTAINER_NAME=dapp
export JAVA_MEMORY_SHARE=35
export PSQL_MEMORY_SHARE=35
export POSTCHAIN_DEBUG=false
export POSTCHAIN_RUNNING_IN_CONTAINER=true
export LOG4J_CONFIGURATION_FILE=/opt/chromaway/postchain/log4j2.yml

trap 'echo "Shutting down..." ; if declare -p POSTCHAIN_PID &> /dev/null; then kill ${POSTCHAIN_PID}; fi ; su postgres -c "pg_ctl stop -m smart"; exit' TERM INT

echo "Configuring and starting Postgres"
su postgres -c "bash postgres-entrypoint.sh postgres"

# Start postchain with control of stdout
echo 1 > "$POSTCHAIN_STDOUT_CONTROL_FILE"
echo "Starting postchain"
set +e
java -Duser.language=en -Duser.country=US -XX:+UnlockDiagnosticVMOptions -XX:AbortVMOnException=java.lang.OutOfMemoryError \
  -XX:MaxRAMPercentage=$JAVA_MEMORY_SHARE \
  -classpath "$POSTCHAIN_DIR/libs/*:$POSTCHAIN_CLASSPATH_DIR/*" \
  net.postchain.server.AppKt run-node --chain-id 0 \
  --node-config "$POSTCHAIN_DIR/node-config.properties" \
  --blockchain-config "$POSTCHAIN_DIR/manager.xml" | while IFS= read -r line; do if grep -q 1 "$POSTCHAIN_STDOUT_CONTROL_FILE"; then echo $line; fi; done &

while ! POSTCHAIN_PID="$(pgrep java)"; do echo "Waiting for postchain pid"; sleep 0.2; done

# Wait for postchain to startup and retrieve the management chain brid
while ! MANAGEMENT_CHAIN_BRID=$(curl -s http://localhost:7740/brid/iid_0 | grep -e "^[0-9A-F]*$"); do sleep 1; done
set -e

# Hide postchain output while setting up network
echo 0 > $POSTCHAIN_STDOUT_CONTROL_FILE

echo
echo "Postchain is started and the management chain brid is $MANAGEMENT_CHAIN_BRID"
echo

./setup-network.sh "$MANAGEMENT_CHAIN_BRID" "$CONTAINER_NAME"
./setup-extensions.sh
./setup-dapps.sh
./setup-post-init-scripts.sh
./setup-summary.sh

# Show postchain output again and wait for process
echo 1 > $POSTCHAIN_STDOUT_CONTROL_FILE
wait ${POSTCHAIN_PID}
