#!/bin/bash

echo "##########################################################################"
echo "##########################################################################"
echo
echo "Node started successfully"
echo
echo "!! This is a test node - DO NOT USE IN PRODUCTION !!"
echo
echo "Versions:"
echo "  Postchain      : $POSTCHAIN_VERSION"
echo "  Directory chain: $DIRECTORY_CHAIN_VERSION"
echo
echo "Provider and container owner:"
echo "  public key : $(pmc config --get pubkey)"
echo "  private key: $(grep -E ^privkey .chromia/config | grep -Eo "[0-9A-F]*$")"
echo
echo "Blockchains:"
pmc blockchains | jq -r '.[] | "  " + .Rid + "    " + .Name'
echo
echo "Containers:"
pmc containers | jq -r '.[] | "  " + .Name'
echo
echo "Extensions available:"
pmc subnode-images | jq -r '.[] | "  " + .Name'
echo
echo "##########################################################################"
echo "##########################################################################"
