#!/bin/bash

set -eu
shopt -s nullglob

echo "- Deploying new dapps"

for file in "$DAPP_CONFIGS"/*; do

  basename="$(basename "$file")"
  dapp_name="${basename%.*}"

  echo "  - Found dapp: $dapp_name"

  dapp_brid="$(pmc blockchains | jq -r ".[] | select(.Name == \"${dapp_name}\") | .Rid")"
  if [ -z "$dapp_brid" ]; then

    echo "    - Adding new chain: $dapp_name"

    pmc blockchain add -bc "$file" -c dapp -n "$dapp_name"
  else
    echo "    - Dapp $dapp_name already exists, skipping"
  fi
done
