# PQC / Dilithium

### How to deploy a network with PQC/Dilithium support

1. Use the following images to run this example:
```shell
registry.gitlab.com/chromaway/postchain-chromia/chromaway/chromia-server:3.13.15.3801-pqc
registry.gitlab.com/chromaway/postchain-chromia/chromaway/chromia-subnode:3.13.15.3801-pqc
```

For `management-console` use the following dist package or image:
```shell
# management-console-3.16.2.326-pqc-dist.tar.gz
https://gitlab.com/chromaway/core-tools/management-console/-/packages/20099779
# image
registry.gitlab.com/chromaway/core-tools/management-console/pmc:3.16.2.326-pqc
```

For `chr` use the following dist package or image:
```shell
# chromia-cli-0.13.1.957-pqc-dist.tar.gz
https://gitlab.com/chromaway/core-tools/chromia-cli/-/packages/20103892
# image
registry.gitlab.com/chromaway/core-tools/chromia-cli/chr:0.13.1.957-pqc
```

To build PQC versions of packages/images on your own use `pqc/dev` branch of `postchain-chromia`, `management-console`, `chromia-cli` repos. Version format is `<latest_semver>.<build_number>-pqc`, e.g. `3.13.15.3801-pqc`.

2. Add the param to the `config/common.properties` file:
```properties
cryptosystem=net.postchain.crypto.pqc.dilithium.DilithiumCryptoSystem
```

3. Generate Dilithium key pair for node0 using `pmc/chr` and include this file in `config/config.0.properties`:
```shell
pmc keygen --dilithium --node --save dilithium0.key
```

4. Generate Dilithium key pair for alpha provider using `pmc/chr` and add it to the file in `provider/alpha/.chromia/config`:
```shell
pmc keygen --dilithium --node --save dilithium.alpha.key
```

5. Add the param to the `provider/alpha/.chromia/config` file:
```properties
cryptosystem=net.postchain.crypto.pqc.dilithium.DilithiumCryptoSystem
```
or define an environment variable 
```shell
export POSTCHAIN_CRYPTO_SYSTEM=net.postchain.crypto.pqc.dilithium.DilithiumCryptoSystem
```

6. Set the following env vars and run a setup script:
```shell
export INITIAL_PROVIDER=$(pmc config --get pubkey --file provider/alpha/.chromia/config)
export GENESIS_NODE=<node0_pubkey>
export GENESIS_HOST_NAME=...
export GENESIS_API_URL=...

./scripts/setup.sh
```

7. Start a node0 and chain0 on it (this example uses grpc/admin interface to interact with a node):

Start the node0
```shell
docker run --name postchain0 \
    --volume /var/run/docker.sock:/var/run/docker.sock \
    --mount type=bind,source=/var/lib/chromaway/postchain/subnode,target=/var/lib/chromaway/postchain/subnode \
    --mount type=bind,source="$(pwd)/config",target=/config,readonly \
    --mount type=bind,source="$(pwd)/directory1/rell/build",target=/build,readonly \
    -e POSTCHAIN_DEBUG=true \
    -p 9870:9870/tcp \
    -p 9880:9880/tcp \
    -p 7740:7740/tcp \
    -p 50051:50051/tcp \
    registry.gitlab.com/chromaway/postchain-chromia/chromaway/chromia-server:3.13.15.3801-pqc \
    run-server --node-config /config/config.master.0.properties
```

Initialize and/or start the chain0
```shell
docker run --rm \
    --mount type=bind,source="$(pwd)"/config,target=/opt/chromaway/postchain/config,readonly \
    --mount type=bind,source="$(pwd)"/directory1/rell/build/,target=/opt/chromaway/postchain/directory1,readonly \
    registry.gitlab.com/chromaway/postchain-chromia/chromaway/chromia-server:3.13.15.3801-pqc \
    admin blockchain initialize -t 172.17.0.1:50051 -cid 0 -bc ./directory1/manager.xml
```

Or just start it if node was restarted
```shell
docker run --rm \
	registry.gitlab.com/chromaway/postchain-chromia/chromaway/chromia-server:3.13.15.3801-pqc \
	admin blockchain start -t 172.17.0.1:50051 -cid 0
```

8. Initialize Directory1 network:
```shell
pmc network initialize --system-anchoring-config directory1/rell/build/system_anchoring.xml --cluster-anchoring-config directory1/rell/build/cluster_anchoring.xml
```

9. Quick check if a block signature is a Dilithium signature (comparing block signature size of the one-node network)
```shell
psql> select length(block_witness) from devnet_pqc0."c0.blocks" limit 1;
# Dilithium: 3768 = 4 (nmbr of sigs) + 4 (length of key) + 1336 (key) + 4 (length of sig) + 2420 (sig)
# ECDSA: 109 = 4 + 4 + 33 + 4 + 64
```

### Optional: add the second node1 to the network

10. Start the node1
```shell
docker run --name postchain1 \
    --volume /var/run/docker.sock:/var/run/docker.sock \
    --mount type=bind,source=/var/lib/chromaway/postchain/subnode,target=/var/lib/chromaway/postchain/subnode \
    --mount type=bind,source="$(pwd)/config",target=/config,readonly \
    --mount type=bind,source="$(pwd)/directory1/rell/build",target=/build,readonly \
    -e POSTCHAIN_DEBUG=true \
    -p 9871:9870/tcp \
    -p 9881:9881/tcp \
    -p 7741:7740/tcp \
    -p 50052:50051/tcp \
    registry.gitlab.com/chromaway/postchain-chromia/chromaway/chromia-server:3.13.15.3801-pqc \
    run-server --node-config /config/config.master.1.properties
```

11. Initialize and/or start chain0 on node1
```shell
docker run --rm \
    --mount type=bind,source="$(pwd)"/config,target=/opt/chromaway/postchain/config,readonly \
    --mount type=bind,source="$(pwd)"/directory1/rell/build/,target=/opt/chromaway/postchain/directory1,readonly \
    registry.gitlab.com/chromaway/postchain-chromia/chromaway/chromia-server:3.13.15.3801-pqc \
    admin blockchain initialize -t 172.17.0.1:50052 -cid 0 -bc ./directory1/manager.xml

# OR

docker run --rm \
	registry.gitlab.com/chromaway/postchain-chromia/chromaway/chromia-server:3.13.15.3801-pqc \
	admin blockchain start -t 172.17.0.1:50052 -cid 0

```

12. Register provider `beta`
```shell
alpha> pmc provider register -pk $(pmc config --get pubkey --file provider/beta/.chromia/config) -sp -e
beta> pmc node register -h 172.30.0.1 -p 9871 --api-url http://172.30.0.1:7741 -clu 1 -c system -pk <node1_pubkey>
```

13. Start a test blockchain `cities`
```shell
alpha> pmc container add --name c1 --cluster system --pubkeys $(pmc config --get pubkey --file provider/alpha/.chromia/config),$(pmc config --get pubkey --file provider/beta/.chromia/config)
alpha> pmc container info -n c1
alpha> pmc blockchain add -bc ./dapp/build/0.xml -c c1 -n cities
beta> pmc proposal vote --id 57
```

### Querying and posting tx signed by Dilithium 

14. Posting/querying D1 using `pmc`:
```shell
alpha> pmc provider update -n alpha1
alpha> pmc providers
```

15. Posting/querying D1 using `chr`:
```shell
docker run --rm \
	-v $(pwd):/usr/app \
	registry.gitlab.com/chromaway/core-tools/chromia-cli/chr:0.13.1.957-pqc \
	chr tx -cfg ./provider/alpha/.chromia/config \
	--api-url http://172.30.0.1:7740 \
	--blockchain-rid 051F757605E07372458B8E440229C060597E28E2BD69489C84BAF87359AA6499 \
	--secret provider/alpha/.chromia/config \
	update_provider $(pmc config --get pubkey --file provider/alpha/.chromia/config) alpha2 null

docker run --rm \
	-v $(pwd):/usr/app \
	registry.gitlab.com/chromaway/core-tools/chromia-cli/chr:0.13.1.957-pqc \
	chr query --api-url http://172.30.0.1:7740 --blockchain-rid 051F757605E07372458B8E440229C060597E28E2BD69489C84BAF87359AA6499 \
	get_all_providers
```

16. The end of example


### Dependency map

```shell
            ┌───────────────────────────────────────────────────────────────┐
            │ postchain-3.13.18                                             │
            │ (dev)                                                         │
            └───────────▲────────────────────────────▲────────────▲─────────┘
                        │                            │            │
                        │                            │            │
┌───────────────────────┴────────────┐  ┌────────────┴───────┐ ┌──┴─────────────────┐  ┌───────────────────┐
│ postchain-chromia-3.13.15.3801-pqc │  │ pmc-3.16.2.326-pqc │ │ chr-0.13.1.957-pqc │  │ directory1-1.26.2 │
│ (pqc/dev)                          │  │ (pqc/dev)          │ │ (pqc/dev)          │  │ (dev)             │
└────────────────────────────────────┘  └─────────▲──────────┘ └───────────▲────────┘  └────────▲──────────┘
                                                  │                        │                    │
                                                  │                        │                    │
                                          ┌───────┴────────────────────────┴────────────────────┴────┐
                                          │ directory1-example-0.2.6                                 │
                                          │ (dev)                                                    │
                                          └──────────────────────────────────────────────────────────┘
```