## Initializing the network

Start the genesis node and three other nodes

```shell
$ docker run -d --name postchain0 \
    --mount type=bind,source="$(pwd)/config",target=/config,readonly \
    --mount type=bind,source="$(pwd)/directory-chain/build",target=/build,readonly \
    -e POSTCHAIN_DEBUG=true \
    -p 9870:9870/tcp \
    -p 7740:7740/tcp \
    -p 7750:7750/tcp \
    registry.gitlab.com/chromaway/postchain-chromia/chromaway/chromia-server:3.25.0 \
    run-node --node-config /config/config.0.properties --chain-id 0 --blockchain-config /build/mainnet.xml
```

```shell
$ docker run -d --name postchain1 \
    --mount type=bind,source="$(pwd)/config",target=/config,readonly \
    --mount type=bind,source="$(pwd)/directory-chain/build",target=/build,readonly \
    -e POSTCHAIN_DEBUG=true \
    -p 9871:9870/tcp \
    -p 7741:7740/tcp \
    -p 7751:7750/tcp \
    registry.gitlab.com/chromaway/postchain-chromia/chromaway/chromia-server:3.25.0 \
    run-node --node-config /config/config.1.properties --chain-id 0 --blockchain-config /build/mainnet.xml
```

```shell
$ docker run -d --name postchain2 \
    --mount type=bind,source="$(pwd)/config",target=/config,readonly \
    --mount type=bind,source="$(pwd)/directory-chain/build",target=/build,readonly \
    -e POSTCHAIN_DEBUG=true \
    -p 9872:9870/tcp \
    -p 7742:7740/tcp \
    -p 7752:7750/tcp \
    registry.gitlab.com/chromaway/postchain-chromia/chromaway/chromia-server:3.25.0 \
    run-node --node-config /config/config.2.properties --chain-id 0 --blockchain-config /build/mainnet.xml
```

```shell
$ docker run -d --name postchain3 \
    --mount type=bind,source="$(pwd)/config",target=/config,readonly \
    --mount type=bind,source="$(pwd)/directory-chain/build",target=/build,readonly \
    -e POSTCHAIN_DEBUG=true \
    -p 9873:9870/tcp \
    -p 7743:7740/tcp \
    -p 7753:7750/tcp \
    registry.gitlab.com/chromaway/postchain-chromia/chromaway/chromia-server:3.25.0 \
    run-node --node-config /config/config.3.properties --chain-id 0 --blockchain-config /build/mainnet.xml
```

Alpha will then initialize the network and economy chain on node 0
```shell
$ ./scripts/setup-pmc.sh
$ cd provider/alpha
alpha$ pmc network initialize --system-anchoring-config ../../directory-chain/build/system_anchoring.xml --cluster-anchoring-config ../../directory-chain/build/cluster_anchoring.xml
alpha$ pmc network initialize-economy-chain --economy-chain-config ../../directory-chain/build/economy_chain.xml
```

Verify that the network is initialized by noting non-zero values from
```shell
alpha$ pmc network summary
```

## Optional steps

### Optional - EVM transaction submitter chain

Initialize EVM transaction submitter:
```shell
alpha$ pmc network initialize-evm-transaction-submitter-chain --transaction-submitter-config <config file>       
```

### Optional - Price oracle chain

Initialize Price oracle chain:
```shell       
alpha$ pmc network initialize-price-oracle-chain --price-oracle-config <config file>
```

### Optional - Create cluster

Create tag and cluster managed by economy chain:
```shell
alpha$ pmc economy add-tag --name my-tag --scu-price 1 --extra-storage-price 1
alpha$ pmc economy add-cluster -n my-cluster -vs SYSTEM_P -g SYSTEM_P -t my-tag -es 0 -clu 1 
```

Make sure it gets created successfully:
```shell
alpha$ pmc economy cluster-creation-status -n my-cluster 
alpha$ pmc economy clusters 
```


## Inviting providers

Alpha invites all other providers to be system providers

```shell
alpha$ pmc provider register --batch -sp --enable \
  --provider '["pubkey": x"'$(pmc config --get pubkey --file ../beta/.chromia/config)'", "name": "beta"]' \
  --provider '["pubkey": x"'$(pmc config --get pubkey --file ../delta/.chromia/config)'", "name": "delta"]' \
  --provider '["pubkey": x"'$(pmc config --get pubkey --file ../gamma/.chromia/config)'", "name": "gamma"]'
```

## Adding nodes

Let each provider add their node. Check /_debug to verify that node has become validator for both chain0 and anchoring chain before proceeding to next node.

```shell
alpha$ cd ../beta
beta$ pmc config --set api.url=http://localhost:7740
beta$ pmc node add --pubkey 035676109c54b9a16d271abeb4954316a40a32bcce023ac14c8e26e958aa68fba9 --host $(find-ip) --port 9871 --api-url http://localhost:7741 --cluster system --territory SE
```

```shell
beta$ ../gamma
gamma$ pmc config --set api.url=http://localhost:7740
gamma$ pmc node add --pubkey 03f811d3e806e6d093a4bcce49c145ba78f9a4b2fbd167753ecab2a13530b081f8 --host $(find-ip) --port 9872 --api-url http://localhost:7742 --cluster system --territory SE
```

```shell
gamma$ ../delta
delta$ pmc config --set api.url=http://localhost:7740
delta$ pmc node add --pubkey 03ef3f5be98d499b048ba28b247036b611a1ced7fcf87c17c8b5ca3b3ce1ee23a4 --host $(find-ip) --port 9873 --api-url http://localhost:7743 --cluster system --territory SE
```
