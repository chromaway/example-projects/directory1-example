# Single provider setup

In this example we will use a single provider setup to manage the configuration updates of a dapp. This is useful when developing a dapp locally.

## Initialization

Start a node using (for instructions on how to run a node with master-sub infrastructure, please see [Master-sub infrastructure](master-sub.md)):

```shell
$ docker run --name postchain0 \
    --mount type=bind,source="$(pwd)/config",target=/config,readonly \
    --mount type=bind,source="$(pwd)/directory-chain/build",target=/build,readonly \
    -e POSTCHAIN_DEBUG=true \
    -p 9870:9870/tcp \
    -p 7740:7740/tcp \
    -p 7750:7750/tcp \
    registry.gitlab.com/chromaway/postchain-chromia/chromaway/chromia-server:3.25.0 \
    run-node --node-config /config/config.0.properties --chain-id 0 --blockchain-config /build/manager.xml
```

This will start a node with initial blockchain running using Docker. In this guide we will communicate with the node through the REST API configured on port 7740 as defined in `config.0.properties`.

```shell
$ ./scripts/setup-pmc.sh
$ pmc network initialize --system-anchoring-config directory-chain/build/system_anchoring.xml --cluster-anchoring-config directory-chain/build/cluster_anchoring.xml
```

The host and port used is the external ip and port that other nodes can communicate with the initial node. 
In this example, we only use a single node, so this can be set to any non-null values, but it is better to use real values if more nodes are added at a later stage.

Check that the network is indeed initialized 

```shell
$ pmc network summary
```

This command should show non-zero values.

You will now have a `system` cluster running on a single node and a `system` container:

```shell
$ pmc clusters                    // list all clusters
$ pmc cluster info --name system  // Show information about cluster
$ pmc containers                  // list all containers
```

## Launch a blockchain

You can deploy your dapp directly to the system container, but in this tutorial we will create a new container to contain resource usage of the dapp.

Create a new container

```shell
$ pmc container add --name cities --cluster system --pubkeys $(pmc config --get pubkey --file provider/alpha/.chromia/config)
```

and see that is was added using

```shell
$ pmc containers
```

Launch a new blockchain using the `pmc blockchain add` command. A test blockchain configuration should be found in `app-out` folder after running the setup script. 

```shell
$ CITY_DAPP_BRID=$(pmc blockchain add --quiet --name city_tracker --container cities --blockchain-config app/build/city.xml)
```

> **Note**: The output will now say "Blockchain city_tracker has been proposed", but since you are the only provider in the network, you have indeed also added it.

Check that your blockchain is running

```shell
$ pmc blockchains
```

## Updating a blockchain

Say that you are developing your dapp and wants to add a query "get_hometown" to the city tracker while the app is running. First, make sure this query does not exist by attempting to make this query.
Make the query towards the city tracker app and see how it fails with a `400 Bad Request Unknown query: get_hometown`

```shell
$ cd app
app$ chr query --blockchain-rid $CITY_DAPP_BRID get_hometown
```

Then, add the query to the dapp (`app/main.rell`) and recompile the dapp

```shell
app$ echo 'query get_hometown() = "Lulea";' >> src/main.rell
app$ chr build
```

You can now update the dapp using pmc

```shell
app$ pmc blockchain update -bc build/city.xml --blockchain-rid $CITY_DAPP_BRID
```

Now, the new query should be accessible from the dapp chain

```shell
app$ chr query --blockchain-rid $CITY_DAPP_BRID get_hometown
Query get_hometown returned
"Lulea"
```

> **Note**: You can also use chromia-cli instead of pmc to deploy/update blockchains.
