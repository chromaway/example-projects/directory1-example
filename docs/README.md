# Directory1 Example

In this guide you can learn how to set up your own blockchain network by studying a concrete example. The example project is called "The city tracker".

The management chain (BC0) is set up on the first node and also added by the management client (postchain-mc) so that bc0 will become aware of itself. Then we can in a convenient way update configuration, add a second blockchain to be managed by bc0: ``city``. More nodes can optionally be added and be made signers. This sample provides configurations for up to 4 nodes.

## Content

This sample contains:
```shell
├── app
│   ├── chromia.yml
│   └── src
│       └── main.rell
├── chromia-cli-0.1.2-dist.tar.gz
├── config
│   ├── c0-deploy.xml
│   ├── common.properties
│   ├── config.0.properties
│   ├── config.1.properties
│   ├── config.2.properties
│   ├── config.3.properties
│   ├── docker.db.properties
│   └── localhost.db.properties
├── directory1-3.7.0-SNAPSHOT-sources.tar.gz
├── docs
│   ├── README.md
│   ├── multi-provider.md
│   └── single-provider.md
├── pmc-directory-3.7.0-SNAPSHOT-dist.tar.gz
├── provider
│   ├── alpha
│   ├── beta
│   ├── delta
│   └── gamma
└── scripts
    ├── find-ip
    ├── setup.sh
    └── wipe-db
```

`app` is the city tracker which can be deployed to the network.
`config` contains node configurations for four nodes. By default, they are pointing to `docker.db.properties` which uses `host.docker.internal:5432`. Update this path to the real database that you are using.
`provider` contains configurations for four providers. They are all pointing towards node 0-3 respectively. Change this using `pmc config` command.
`scripts` contains some utility scripts. `find-ip` lets you find your external ip address. Note that in a production environment, the IP of the server running the node must be used. 
`setup.sh` will unpack all tarballs and generate blockchain configurations.

> **Note**: We recommend installing [direnv](https://direnv.net) to easily access all scripts in the scripts folder and the management console directly from path.

## Prerequisites

- docker
- [direnv](https://direnv.net) (this is not mandatory but the examples will assume that all scripts are found on path)

> **Note**: If you are on mac, it is recommended to install `coreutils` to ensure that the scripts in this distribution is working correctly
> ```
> brew install coreutils
> ```

## Setup

Run the script `scripts/setup.sh` to initiate this project. You can manually specify which IP you have and which blockchain to use by 
```shell
IP=<my-ip> BLOCKCHAIN=manager setup.sh
```
The script will unpack the management console as well as the deployment tool and the BC0 sources. It will also generate the blockchain configurations to the `directory-chain/build` and `app/build` folders.

In the configuration for BC0 we have:
```yml
initial_provider: &initial_provider x"03ECD350EEBC617CBBFBEF0A1B7AE553A748021FD65C7C50C5ABB4CA16D4EA5B05"
```

which is configured to the provider called `alpha` (found in providers/alpha directory). This will be the initial provider of the network.

You can also do the setup manually:

1. Untar `directory-chain`, `management-console` and `chromia-cli` tarballs
2. Update `config.yml` and `config/genesis.properties` to use the correct ip and api-url for the genesis node. 
3. Compile the manager chain using chromia cli: `chr build`
4. Compile the example dapp if you want to use it: `cd app && chr build`

### Postgres

You must have a postgres server running to be able to run nodes. You can start a postgres server using docker

```shell
$ docker run --name postgres -e POSTGRES_INITDB_ARGS="--lc-collate=C.UTF-8 --lc-ctype=C.UTF-8 --encoding=UTF-8" -e POSTGRES_PASSWORD=postchain -e POSTGRES_USER=postchain -p 5433:5432 postgres
```

You then need to configure `config/docker.db.properties` to point the postgres instance. 

> **Note**: The host must be the docker host. On mac, this is `host.docker.internal`, whereas on linux/windows, this is typically `172.17.0.1`

### Starting over

If you want to restart and give up your progress, all you need to do is wipe the database. In a production environment, you never want to do this, but when practicing/testing, it can be useful. You can do this in two ways.
1. Delete the postgres container and all its data
2. Wipe the database schema associated with the node in question
```shell
$ wipe-db config/config.0.properties
```

If you are running master-sub infrastructure you also need to delete the folder configured as `container.host-mount-dir`.

## Guides

### Managing dapp configurations on a single node

In the guide [single provider](single-provider.md), a single node is used to deploy and update a dapp.

### Setting up a network managed by multiple providers

The guide [multi provider](multi-provider.md) shows how to add a provider and how a provider can register nodes to a cluster.
