# Inter-Chain Messaging Facility (ICMF) demo

## Setup

Set up a network according to the instructions in this repo (it is required to initialize the network with an anchoring chain config). Could be single node or multi node depending on what you would like to test.

Deploy the sender chain:
```shell
# building
$ cp -R directory-chain/src/messaging icmf/sender/src
$ cli/bin/chr build --settings icmf/sender/config.yml
# creating a container
$ ./scripts/setup-pmc.sh
$ pmc container add --name icmfsender --cluster system --pubkeys $(pmc config --get pubkey --file provider/alpha/.chromia/config)
# deploying
$ export SENDER_BRID=$(pmc blockchain add --quiet --name sender --container icmfsender --blockchain-config icmf/sender/build/sender.xml)
```

Deploy the receiver chain:
```shell
# building
$ sed -i'' -e "s/{senderBrid}/x\"$SENDER_BRID\"/g" icmf/receiver/config.yml
$ cli/bin/chr build --settings icmf/receiver/config.yml
# creating a container
$ pmc container add --name icmfreceiver --cluster system --pubkeys $(pmc config --get pubkey --file provider/alpha/.chromia/config)
# deploying
$ export RECEIVER_BRID=$(pmc blockchain add --quiet --name receiver --container icmfreceiver --blockchain-config icmf/receiver/build/local-specific-receiver.xml)
```

## Send and receive messages
The sender dapp sends ICMF messages with a slightly contrived format (just to make the demo a bit more interesting)
```
message: gtv_string,
subject: gtv_string,
high_priority: gtv_integer (should be intepreted as a boolean)
```

> **Note**: ICMF messages can only be sent between blockchains within the same cluster.

We can trigger sending of a message by posting a tx to sender chain by submitting a tx with the `send_icmf_message` operation.
The first argument to this operation is the icmf-topic to publish the message on. (This example is configured to listen to the topic "L_icmf-test-message").

> **Note**: All topics must start with L_

```shell
$ chr tx --blockchain-rid $SENDER_BRID send_icmf_message L_icmf-test-message hello subject 0 
```
Verify that a message has been sent by the sender dapp by querying it:
````shell
$ chr query --blockchain-rid $SENDER_BRID get_sent_messages
````

Check if the receiver chain has received the message
```shell
$ chr query --blockchain-rid $RECEIVER_BRID get_received_messages 
```
