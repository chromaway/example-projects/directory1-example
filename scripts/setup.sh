#!/bin/bash

set -eu


C0_SOURCES=$(find . -maxdepth 1 -regex '.*directory-chain-[0-9]*.[0-9]*.[0-9]*.*-sources.tar.gz')
PMC=$(find . -maxdepth 1 -regex '.*management-console-[0-9]*.[0-9]*.[0-9]*.*-dist.tar.gz')
CLI=$(find . -maxdepth 1 -regex '.*chromia-cli-[0-9]*.[0-9]*.[0-9]*.*-dist.tar.gz')
tar xf "$C0_SOURCES"
tar xf "$PMC"
mkdir -p cli
tar xf "$CLI" --directory cli
sed -i'' -e "s/{hostName}/${IP:-$(bash scripts/find-ip)}/g" directory-chain/chromia.yml
sed -i'' -e "s/{hostName}/${IP:-$(bash scripts/find-ip)}/g" config/genesis.properties
sed -i'' -e "s/{hostName}/${IP:-$(bash scripts/find-ip)}/g" config/master-sub.properties
sed -i'' -e "s/{apiUrl}/http:\/\/localhost:7740/g" directory-chain/chromia.yml
bash cli/bin/chr install --settings directory-chain/chromia.yml
bash cli/bin/chr build --settings directory-chain/chromia.yml
bash cli/bin/chr build --settings app/chromia.yml

