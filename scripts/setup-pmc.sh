#!/bin/bash

set -eu

export BRID0=$(curl http://localhost:7740/brid/iid_0)
echo "$BRID0"

bash management-console/bin/pmc config --file provider/alpha/.chromia/config --set brid="$BRID0"
bash management-console/bin/pmc config --file provider/beta/.chromia/config --set brid="$BRID0"
bash management-console/bin/pmc config --file provider/gamma/.chromia/config --set brid="$BRID0"
bash management-console/bin/pmc config --file provider/delta/.chromia/config --set brid="$BRID0"
